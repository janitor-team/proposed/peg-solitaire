# -------------------------------------------------
# Project created by QtCreator 2010-07-18T16:54:25
# -------------------------------------------------
TARGET = peg-solitaire
TEMPLATE = app
QT += widgets \
      core
#Només per a QTest
#QT += testlib

CONFIG += warn_on \
    release

#Al windows això cal comentar-ho per què no deixa compilar
!win32 {
QMAKE_CXXFLAGS += -fstack-protector-all --param ssp-buffer-size=1
QMAKE_LFLAGS += -fstack-protector-all --param ssp-buffer-size=1
}
QMAKE_CXXFLAGS += -D_FORTIFY_SOURCE=2 -Wl,-O1 -Wl,relro -Wl,-z,--as-needed -rdynamic -o
win32:QMAKE_LFLAGS += -Wl,--dynamicbase -Wl,--nxcompat

MOC_DIR = build
OBJECTS_DIR = build
RCC_DIR = build
UI_DIR = build
SOURCES += scr/main.cpp \
    scr/frmprincipal.cpp \
    scr/tauler.cpp \
    scr/fitxa.cpp \
    scr/moviment.cpp \
    scr/funcions.cpp \
    scr/rellotge.cpp
HEADERS += scr/frmprincipal.h \
    scr/tauler.h \
    scr/fitxa.h \
    scr/moviment.h \
    scr/funcions.h \
    scr/rellotge.h \
    scr/constants.h
TRANSLATIONS += locales-ts/solitari_en_EN.ts \
    locales-ts/solitari.ts \
    locales-ts/solitari_es_ES.ts \
    locales-ts/solitari_ca_ES.ts \
    locales-ts/solitari_eu_ES.ts\
    locales-ts/solitari_gl_ES.ts \
    locales-ts/solitari_fr_FR.ts \
    locales-ts/solitari_pt_PT.ts \
    locales-ts/solitari_pt_BR.ts \
    locales-ts/solitari_it_IT.ts \
    locales-ts/solitari_de_DE.ts \
    locales-ts/solitari_pl_PL.ts \
    locales-ts/solitari_en_US.ts

RESOURCES += images/resources.qrc

unix:!macx {
    isEmpty(PREFIX):PREFIX = /usr
    target.path = $$PREFIX/games/
    man.path = $$PREFIX/share/man/man6/
    man.files = menu/peg-solitaire.6.gz

    # això només per garantir que s'eliminen els directoris amb uninstall
    help.path = $$PREFIX/share/games/peg-solitaire/help/
    help.files = help/*

    games.path = $$PREFIX/share/games/peg-solitaire/games/
    games.files = games/*
    locales.path = $$PREFIX/share/games/peg-solitaire/locales/
    locales.files = locales/*

    desktop.path = $$PREFIX/share/applications/
    desktop.files = menu/peg-solitaire.desktop
    imatges.path = $$PREFIX/share/games/peg-solitaire/images/
    imatges.files= images/*
    pixmaps.path = $$PREFIX/share/pixmaps/
    pixmaps.files = images/peg-solitaire.xpm

    INSTALLS += help \
        games \
        locales \
        desktop \
        pixmaps \
	imatges \
        man \
        target

}

##########################
# Actualització dels arxius de traducció
# lupdate -verbose peg-solitaire.pro
# lupdate -verbose -noobsolete peg-solitaire.pro
# compilació dels arxius .ts
# lrelease peg-solitaire.pro
#
#
# v. 2.2
# - La recerca iterativa queda implementada als solitaris
# en diagonal i a bona part dels altres. No és eficient
# amb els solitaris amb moltes fitxes (més de 40).
# - Afegits 11 problemes més (i un solitari nou Solo)
# - Incorporats els suggeriments de l'informe d'errors
# #858110 a Debian llevat del que afecte a l'agrupació dels jocs
# en branques a l'arbre de jocs (que queda pendent de valorar
# com fer-ho).
# - S'han arreglat alguns detalls i errors (...que no s'ha documentat)
#
# 04/12/16
# La recerca iterativa amb control de aïllament per conjunts
# ha funcionat.
#
# 01/10/16
# v. 2.1
#
# Afegit Q_GADGET a fitxa.h moviment.h i tauler.h
# per evitar un error en compilar amb cmake
#
# 23/09/16
# Degut a un canvi en la codificació de caràcters, ha calgut
# actualizar els arxius de traducció i eliminar cadenes que han
# quedat obsoletes.
#
# 21/09/16 L'elimininació de les marques personals no funcionava correctament
# Eliminava tota la informació de l'arxiu de configuració
#
# S'ha millorat la recerca de solucions: ara troba solucions
# noves més aviat.
# Afegides imatges 48x48 i 64x64
#
# 12/09/16
# Afegida l'opció de menú web del programa
# i del peces
#
# 14/08/16
# El joc 307 estava traduït com «Solitari estrella»
# enlloc de com «Clàssic» degut a què el codi de traducció
# no era el correcta a l'arxiu data.sol (tenia el 6 enlloc del 1).
# I a l'arxiu de traducció del català faltava la e a
# Weileg - fletxa.
#
# 24/02/2016
# * Migració a Qt 5
# En conseqüència a calgut canviar de
# QHash<int, Fitxa*> m_fitxes
# a QMap<int, Fitxa*> m_fitxes; i
# QHashIterator a QMapIterator. Amb el Qt 5, el
# QHash posa els elements desordenats i cada vegada
# de manera distinta: això feia que el sistema de noms
# dels arxius no funcionava.
# * Als noms dels arxius, s'ha eliminat l'espai buit anterior al codi
# del joc (QString frmPrincipal::nomArxiuJoc(int tipus))
#
# v. 2.0
# Noves traduccions
# brasiler d'Ezequiel BUTZKE
# Polac per Jarosław Jabłoński
#
# 155 nous problemes
#
# 27/01/13
# Canviat el sistema de traducció dels noms
# dels jocs (funció nomDelJoc)
# Les modificacions de jocs del programa
# son tractades com els jocs personalitzats.
# Es diferencian amb les propietats del tauler
# solitariPersonalitzat i solitariModificat. A més
# la propietat de la fitxa p_fitxaSolitariPersonalitzat es
# fa servir per gestionar aquests dos tipus de jocs
#
# 13/01/13
# Solitaris personalitzats
# Tooltip en els arbres dels jocs
#
# 30/12/12
# Els jocs que ja s'han resolt (hi ha un rècord)
# queden marcats amb la imatge OK a l'arbre
#
# 08/12/12
# Els records es mostren a un arxiu HTML
# (no hi caben a un about)
#
# 29/11/2012
# Generació automàtica de nous solitaris
# (solitari a l'atzar)
# Hi ha nous procediments i algunes modificacions més.
#
# 25/07/12
# Afegit a la traducción el text de les
# accions retrocedeix/avança
# (pareix que això no és possible!)
#
# 20/07/12
# Acabada la nova funcionalitat de jocs amb
# final marcat. S'han fet canvis a diverses
# funcions i a les fitxes.
#
# 08/07/12
# Si desprès de marcar un moviment, a continuació
# es feia servir els botons avaçar/retrocedir
# les fitxes del moviment quedaven marcades
# S'ha afegit el procediment
# Moviment::eliminaMovimentMarcat()
#
# 17/06/12
# Modificat el sistema de traducció dels
# noms del jocs.
#
# 02/06/12
# L'elecció del idioma es desa
# a la configuració
#
# v. 1.2
# 24 Noves modalitats del joc
#
# 31/10/11
# Afegit el locale en_US per tancar
# l'informe d'error d'Ubuntu
#
# 21/04/11
# Modificada la funció funcions.directoriLinux() per fer
# compatibles l'aplicació instal·lada i el codi font
#
# 1.1
#
# 06/02/11
# Afegit intentsSolucio per evitar els casos en
# què no es troba la solució
#
# 05/02/11
# Nova funció per mantenir en pausa el joc
#
# 04/02/11
# Quan el programa no aconsegueix trobar
# la solució al joc, llavors s'elimina l'arxiu de
# configuracions eliminades (eliminaArxiuConfgEliminades()).
#
# 02/02/11
# Algunes vegades, la solució ràpida desa
# només alguns moviments. S'ha evitat que
# es desin o carreguin solucions incorrectes.
# Queda per determinar perquè es desaven
# les solucions incorrectament
#
#27/01/11
# Petita modificació per reduir el nombre de vegades
# en què el programa no aconsegueix trobar
# la solució (només passa quan el nombre de
# fitxes és petit)
#
#23/01/2011
# Eliminada una divisió per zero en les modalitats
# inverses.
#
# 1.0.2
#
# 13/01/11
# En fer passar ràpidament el ratolí per l'arbre de selecció
# de jocs, es carregaven incorrectament les solucions
#
# 1.0.1
# * Tancats els bugs de Christina
#
##########################
