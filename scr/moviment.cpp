/***********************************************************************
 *
 * Copyright (C) 2010-2017 Innocent De Marchi <tangram.peces@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/
#include <QtCore>

#include "moviment.h"
#include "tauler.h"
#include "fitxa.h"


Moviment::Moviment(int fitxaInicial, int fitxaBotada, int fitxaFinal, int direccioMoviment,  Tauler *taulerJoc):
    p_fitxaInicial (fitxaInicial),
    p_fitxaBotada(fitxaBotada),
    p_fitxaFinal(fitxaFinal),
    p_direccioMoviment(direccioMoviment),
    p_taulerJoc(taulerJoc) {
    p_direccioMoviment=p_taulerJoc->m_fitxes.value(p_fitxaInicial)->tipusMoviment();

}

/* Fa un moviment. Segons el tipusMoviment, determina
 * l'estat final de les fitxes (inicial, mitjana i final)
 */
void Moviment::redo(){
    if (p_direccioMoviment==1){
        fesMoviment(0,0,1);
   }
    else if (p_direccioMoviment==2){        
        fesMoviment(0,1,1);
   }
    else if (p_direccioMoviment==3){
        fesMoviment(0,0,1);
   }
    else if (p_direccioMoviment==5){
        fesMoviment(0,1,1);
   }
    /* Borram les marques de qualsevol moviment marcat
     * (ja que pot ésser que s'hagi clicat algun dels botons
     * d'endevant/enrrera
     */
    eliminaMovimentMarcat();
    marcaSituacioJoc();
}

void Moviment::undo(){
    //Les fitxes vermelles de final de joc es passen a blaves
    if(p_taulerJoc->p_movimentsUndoStack->index()==
       p_taulerJoc->p_movimentsUndoStack->count() ){
         eliminaFitxesVermelles();
    }
    //direccció normal
    if (p_direccioMoviment==1){
        fesMoviment(1,1,0);
   }
    //moviment invers
    else if (p_direccioMoviment==2){
        fesMoviment(1,0,0);
   }
    //moviments perpendiculars i en diagonal
    else if (p_direccioMoviment==3){
        fesMoviment(1,1,0);
    }
    //moviments perpendiculars i en diagonal invers
    else if (p_direccioMoviment==5){
        fesMoviment(1,0,0);
   }
    /* Borram les marques de qualsevol moviment marcat
     * (ja que pot ésser que s'hagi clicat algun dels botons
     * d'endevant/enrera
     */
    eliminaMovimentMarcat();
    marcaSituacioJoc();
}

void Moviment::fesMoviment(int primera, int segona, int tercera){
       p_taulerJoc->m_fitxes.value(p_fitxaInicial)->setEstat(primera);
       p_taulerJoc->m_fitxes.value(p_fitxaBotada)->setEstat(segona);
       p_taulerJoc->m_fitxes.value(p_fitxaFinal)->setEstat(tercera);
     //  setText(QString("%1 %2 %3").arg(p_fitxaInicial).arg(p_fitxaBotada).arg(p_fitxaFinal));
       setText(movimentACoordenades());
       marcaSituacioJoc();
}

void Moviment::eliminaFitxesVermelles(){
    //QHashIterator <int, Fitxa*> i(p_taulerJoc->m_fitxes);
    QMapIterator <int, Fitxa*> i(p_taulerJoc->m_fitxes);
    while (i.hasNext()) {
        i.next();
        if (i.value()->estat()==5){
            i.value()->setEstat(1);
        }
     }
}


/* Quan es fan moviments enrrera/envant
 * cal assegurar-se que no hi ha cap moviment
 * "marcat"
 */
void Moviment::eliminaMovimentMarcat(){
    //QHashIterator <int, Fitxa*> i(p_taulerJoc->m_fitxes);
    QMapIterator <int, Fitxa*> i(p_taulerJoc->m_fitxes);
    while (i.hasNext()) {
        i.next();
      /* Tipus de moviment normal i diagonal
       */
      if (p_direccioMoviment != 2){
        /* Si la fitxa està seleccionada con inici
         * d'un moviment, la passam a normal
         */
        if (i.value()->estat()==2){
            i.value()->setEstat(1);
        }
        /* Si la fitxa està seleccionada com a final
         * d'un moviment, la passam a normal.
         */
        if (i.value()->estat()==3){
            i.value()->setEstat(0);
        }
     }
     /*
      * Moviment invers
      */
     else {
          /* Si la fitxa està seleccionada com inici
           * d'un moviment, la passam a normal
           */
          if (i.value()->estat()==2){
              i.value()->setEstat(1);
          }
          /* Si la fitxa està seleccionada com a final
           * d'un moviment, la passam a normal
           * (en aquest cas, buida)
           */
          if (i.value()->estat()==3){
              i.value()->setEstat(0);
          }
      }
}
}


QString Moviment::movimentACoordenades(){
    return QString("(%1,%2) -> (%3,%4)").arg(p_fitxaInicial/100).arg(p_fitxaInicial % 100).
                                arg(p_fitxaFinal/100).arg(p_fitxaFinal % 100);
}

void Moviment::marcaSituacioJoc(){
 return;
   int numeroConjunts= p_taulerJoc->solucionaJocActual_FitxesAillades_PerConjunts();
    p_taulerJoc->solucionaJocActual_FitxesAillades1();
    p_taulerJoc->solucionaJocActual_conjuntsAillats(numeroConjunts);
}
