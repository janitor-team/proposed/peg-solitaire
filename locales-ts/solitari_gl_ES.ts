<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="gl_ES">
<context>
    <name>Traduccio</name>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="817"/>
        <source>English</source>
        <translatorcomment>Nombre del idioma de la traducción</translatorcomment>
        <translation>Galego</translation>
    </message>
</context>
<context>
    <name>frmPrincipal</name>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="79"/>
        <location filename="../scr/frmprincipal.cpp" line="214"/>
        <location filename="../scr/frmprincipal.cpp" line="486"/>
        <location filename="../scr/frmprincipal.cpp" line="875"/>
        <location filename="../scr/frmprincipal.cpp" line="3971"/>
        <source>Solitari</source>
        <translation>Solitario</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="140"/>
        <location filename="../scr/frmprincipal.cpp" line="500"/>
        <location filename="../scr/frmprincipal.cpp" line="2752"/>
        <source>Solucions</source>
        <translation>Solucións</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="215"/>
        <location filename="../scr/frmprincipal.cpp" line="3972"/>
        <source>No s&apos;ha trobat l&apos;arxiu %1</source>
        <translation>Non se atopou o arquivo %1</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="123"/>
        <location filename="../scr/frmprincipal.cpp" line="499"/>
        <location filename="../scr/frmprincipal.cpp" line="3794"/>
        <source>Modalitats del joc</source>
        <translatorcomment>Juegos</translatorcomment>
        <translation>Xogos</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3808"/>
        <source>Solitari 3x5</source>
        <translation>Solitário 3x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3809"/>
        <location filename="../scr/frmprincipal.cpp" line="3847"/>
        <source>Triangular 4x7</source>
        <translation>Triangular 4x7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3819"/>
        <source>Quadrat 5x5</source>
        <translation>Cadrado 5x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3825"/>
        <source>Wiegleb</source>
        <translation>Wiegleb</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3826"/>
        <location filename="../scr/frmprincipal.cpp" line="3891"/>
        <source>Diamant 9x9</source>
        <translatorcomment>Diamante 9x9</translatorcomment>
        <translation>Diamante 9x9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3831"/>
        <location filename="../scr/frmprincipal.cpp" line="3845"/>
        <source>Quadrat 6x6</source>
        <translation>Cadrado 6x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3832"/>
        <source>Diamant 5x5</source>
        <translation>Diamante 5x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3833"/>
        <source>Diamant 7x7</source>
        <translation>Diamante 7x7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3835"/>
        <source>Incomplet 6x6</source>
        <translation>Incompleto 6x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3836"/>
        <source>Incomplet 7x7</source>
        <translation>Incompleto 7x7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3837"/>
        <location filename="../scr/frmprincipal.cpp" line="3868"/>
        <source>Wiegleb reduit</source>
        <translation>Wiegleb reducido</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3838"/>
        <source>Solitari 8x9</source>
        <translation>Solitario 8x9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3839"/>
        <location filename="../scr/frmprincipal.cpp" line="3844"/>
        <source>Solitari 5x6</source>
        <translation>Solitario 5x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3851"/>
        <location filename="../scr/frmprincipal.cpp" line="3880"/>
        <source>Solitari 7x5</source>
        <translation>Solitario 7x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3853"/>
        <source>Quadrat 9x9</source>
        <translation>Cadrado 9x9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3855"/>
        <source>Triangular 5</source>
        <translation>Trianguilar 5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3856"/>
        <source>Triangular 4</source>
        <translation>Trianguilar 4</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3857"/>
        <source>Triangular 6</source>
        <translation>Trianguilar 6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3869"/>
        <source>Solitari 3x5 bis</source>
        <translation>Solitario 3x5 bis</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3870"/>
        <source>Solitari 4x4</source>
        <translation>Solitario 4x4</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3871"/>
        <source>Solitari 6x5</source>
        <translation>Solitario 6x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3872"/>
        <source>Solitari 4x5</source>
        <translation>Solitario 4x5</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3873"/>
        <source>Triangular 7</source>
        <translation>Trianguilar 7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3874"/>
        <source>Triangular 8</source>
        <translation>Trianguilar 8</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3875"/>
        <source>Triangular 9</source>
        <translation>Trianguilar 9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3877"/>
        <source>Clàssic - molinet</source>
        <translation>Clásico - muíño</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3878"/>
        <source>Triangular 10</source>
        <translation>Trianguilar 10</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3879"/>
        <source>Quadrat 8x8</source>
        <translation>Cadrado 8x8</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="154"/>
        <location filename="../scr/frmprincipal.cpp" line="501"/>
        <location filename="../scr/frmprincipal.cpp" line="3496"/>
        <location filename="../scr/frmprincipal.cpp" line="3648"/>
        <location filename="../scr/frmprincipal.cpp" line="3692"/>
        <source>Jocs personalitzats</source>
        <translation>Xogos personalizados</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3801"/>
        <source>Solitari estrella 7x7</source>
        <translation>Solitario estrela</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3813"/>
        <source>Solitari 6x7</source>
        <translation>Solitario 6x7</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="464"/>
        <location filename="../scr/frmprincipal.cpp" line="516"/>
        <source>&amp;Surt</source>
        <translation>&amp;Pecha</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="465"/>
        <source>Veure records</source>
        <translation>Ver marcas persoais</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="466"/>
        <location filename="../scr/frmprincipal.cpp" line="484"/>
        <location filename="../scr/frmprincipal.cpp" line="571"/>
        <location filename="../scr/frmprincipal.cpp" line="650"/>
        <source>Ajuda</source>
        <translation>Axuda</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="467"/>
        <location filename="../scr/frmprincipal.cpp" line="568"/>
        <source>Credits</source>
        <translation>Créditos</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="468"/>
        <location filename="../scr/frmprincipal.cpp" line="592"/>
        <source>Agraïments</source>
        <translation>Agradecementos</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="469"/>
        <location filename="../scr/frmprincipal.cpp" line="574"/>
        <source>Web del programa</source>
        <translation>Web do programa</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="470"/>
        <location filename="../scr/frmprincipal.cpp" line="577"/>
        <source>Web del tangram</source>
        <translation>Web do tangram</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="474"/>
        <source>Avança</source>
        <translation>Avanzar</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="491"/>
        <location filename="../scr/frmprincipal.cpp" line="562"/>
        <source>Inici solució</source>
        <translation>Inicio da solución</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="532"/>
        <location filename="../scr/frmprincipal.cpp" line="714"/>
        <source>Pausa</source>
        <translation>Pausa</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="533"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="714"/>
        <source>Continua</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2972"/>
        <source>Joc resolt!</source>
        <translation>Xogo resolto!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2975"/>
        <source>Nova marca personal</source>
        <translation>Nova marca persoal</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2976"/>
        <source>Heu establert un nou record personal en aquesta modalitat de joc</source>
        <translation>Conseguiu unha nova marca persoal nesta modalidade do xogo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2986"/>
        <location filename="../scr/frmprincipal.cpp" line="2991"/>
        <source>Moviment %1 de %2 </source>
        <translation>Movemento %1 de %2</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="481"/>
        <location filename="../scr/frmprincipal.cpp" line="631"/>
        <source>Programa</source>
        <translation>Programa</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="482"/>
        <location filename="../scr/frmprincipal.cpp" line="643"/>
        <source>&amp;Moviments joc</source>
        <translation>&amp;Movementos do xogo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="483"/>
        <location filename="../scr/frmprincipal.cpp" line="647"/>
        <source>Idioma</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="517"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="488"/>
        <location filename="../scr/frmprincipal.cpp" line="521"/>
        <source>Veure marques personals</source>
        <translation>Ver as marcas persoais</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="424"/>
        <location filename="../scr/frmprincipal.cpp" line="3614"/>
        <source>Joc carregat. El vostre record actual és:  %1</source>
        <translation>Xogo cargado. A vosa marca persoal actual é: %1</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="489"/>
        <location filename="../scr/frmprincipal.cpp" line="524"/>
        <source>Elimina marques personals</source>
        <translation>Elimina as marcas persoais</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="490"/>
        <location filename="../scr/frmprincipal.cpp" line="527"/>
        <source>Reinicia el joc actual</source>
        <translation>Reinicia o xogo actual</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="528"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="554"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="559"/>
        <source>Shift+Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3810"/>
        <source>Europeu</source>
        <translatorcomment>Europeo</translatorcomment>
        <translation>Europeo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3823"/>
        <source>Quadrat 5x5 - central</source>
        <translatorcomment>Cuadrado 5x5 - central</translatorcomment>
        <translation>Cadrado 5x5 - central</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3824"/>
        <source>Quadrat 5x5 - H</source>
        <translatorcomment>Cuadrado 5x5 - H</translatorcomment>
        <translation>Cadrado 5x5 - H</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3827"/>
        <source>Europeu - creu</source>
        <translatorcomment>Europeo - cruz</translatorcomment>
        <translation>Europeo - cruz</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3846"/>
        <source>Quadrat 5x5 - quadrats</source>
        <translatorcomment>Cuadrado 5x5 -  cuadrados</translatorcomment>
        <translation>Cadrado 5x5 -  cadrados</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3848"/>
        <source>Triangular 4x7 - quadrat</source>
        <translatorcomment>Triangular 4x7 - cuadrado</translatorcomment>
        <translation>Triangular 4x7 - cadrado</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3858"/>
        <source>Wiegleb - creu petita</source>
        <translatorcomment>Wiegleb - cruz pequeña</translatorcomment>
        <translation>Wiegleb - cruz pequena</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3859"/>
        <source>Wiegleb - simetria</source>
        <translatorcomment>Wiegleb - simetria</translatorcomment>
        <translation>Wiegleb - simetria</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3882"/>
        <source>Clàssic - O</source>
        <translation>Clásico - O</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3884"/>
        <source>Dos quadrats 10x10</source>
        <translatorcomment>Dos cuadrados 10x10</translatorcomment>
        <translation>Dous cadrados 10x10</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3885"/>
        <source>Dos quadrats 11x11</source>
        <translatorcomment>Dos cuadrados 11x11</translatorcomment>
        <translation>Dous cadrados 11x11</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3886"/>
        <source>Tres quadrats 16x16</source>
        <translatorcomment>Tres cuadrados 16x16</translatorcomment>
        <translation>Tres cadrados 16x16</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3887"/>
        <source>Dos quadrats 9x9</source>
        <translatorcomment>Dos cuadrados 9x9</translatorcomment>
        <translation>Dous cadrados 9x9</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3888"/>
        <source>Tres quadrats 13x13</source>
        <translatorcomment>Tres cuadrados 13x13</translatorcomment>
        <translation>Tres cadrados 13x13</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3889"/>
        <source>Quatre quadrats 13x13</source>
        <translatorcomment>Cuatro cuadrados 13x13</translatorcomment>
        <translation>Catro cadrados 13x13</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3890"/>
        <source>Clàssic ampliat</source>
        <translatorcomment>Clásico ampliado</translatorcomment>
        <translation>Clásico - ampliado</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3892"/>
        <source>Rombe 36</source>
        <translatorcomment>Rombo 36</translatorcomment>
        <translation>Rombo 36</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3897"/>
        <source>Hexagonal 7x11</source>
        <translatorcomment>Hexagonal 7x11</translatorcomment>
        <translation>Hexagonal 7x11</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3905"/>
        <source>Solitari a l&apos;atzar</source>
        <translation>Solitario ao azar</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3818"/>
        <source>Solitari OK</source>
        <translation>Solitari OK</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="883"/>
        <source>Sota llicència GPL 2.0 o posterior</source>
        <translation>Baixo licenza GPL 3.0 ou posterior</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1008"/>
        <source>De debó voleu eliminar les vostres marques?</source>
        <translation>Seguro que quere eliminar as súas marcas persoais?</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1164"/>
        <location filename="../scr/frmprincipal.cpp" line="1170"/>
        <location filename="../scr/frmprincipal.cpp" line="1530"/>
        <location filename="../scr/frmprincipal.cpp" line="2129"/>
        <source>Cercant solució</source>
        <translation>Buscando solución</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1368"/>
        <location filename="../scr/frmprincipal.cpp" line="1650"/>
        <location filename="../scr/frmprincipal.cpp" line="1656"/>
        <source>No ha estat possible trobar una solució!</source>
        <translation>Non se atopou unha solución!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2620"/>
        <location filename="../scr/frmprincipal.cpp" line="2670"/>
        <source>S&apos;ha trobat una nova solució!</source>
        <translation>Atopouse unha nova solución!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2721"/>
        <source>Solució %1</source>
        <translation>Solución %1</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2762"/>
        <source>De debó voleu carregar la solució? Perdreu els moviments que heu fet!</source>
        <translation>Seguro que quere cargar a solución? Vai perder os movementos realizados!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2769"/>
        <source>. Feu servir els botons Avança i Retrocedeix per veure la solució. </source>
        <translation>. Utilize os botóns Avanza e Retrocede para ver a solución.</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2897"/>
        <source> de %1</source>
        <translation> de %1</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2969"/>
        <source>No hi ha més moviments: el joc ha finalitzat!</source>
        <translation>Non hai máis movementos: finalizou o xogo!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2997"/>
        <source>No hi ha moviments!</source>
        <translation>Non hai movementos!</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3795"/>
        <source>Clàssic</source>
        <translation>Clásico</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3796"/>
        <source>Clàssic - simetria</source>
        <translatorcomment>Clásico - simetria</translatorcomment>
        <translation>Clásico - simetria</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3797"/>
        <source>Clàssic - pentàgon</source>
        <translatorcomment>Clásico - pentágono</translatorcomment>
        <translation>Clásico - pentágono</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3798"/>
        <source>Clàssic - creu petita</source>
        <translatorcomment>Clásico - cruz pequeña</translatorcomment>
        <translation>Clásico - cruz pequena</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3799"/>
        <source>Clàssic - creu gran</source>
        <translatorcomment>Clásico - cruz grande</translatorcomment>
        <translation>Clásico - cruz grande</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3802"/>
        <source>Clàssic - superior</source>
        <translatorcomment>Clásico - superior</translatorcomment>
        <translation>Clásico - superior</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3803"/>
        <source>Clàssic - inferior</source>
        <translatorcomment>Clásico - inferior</translatorcomment>
        <translation>Clásico - inferior</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3804"/>
        <source>Clàssic - fletxa</source>
        <translatorcomment>Clásico - flecha</translatorcomment>
        <translation>Clásico - frecha</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3805"/>
        <source>Clàssic - piràmide</source>
        <translatorcomment>Clásico - pirámide</translatorcomment>
        <translation>Clásico - pirámide</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3806"/>
        <source>Clàssic - diamant</source>
        <translatorcomment>Clásico - diamante</translatorcomment>
        <translation>Clásico - diamante</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3807"/>
        <source>Clàssic - rombe</source>
        <translation>Clássico - rombo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3811"/>
        <location filename="../scr/frmprincipal.cpp" line="3815"/>
        <location filename="../scr/frmprincipal.cpp" line="3863"/>
        <source>Asimètric 8x8</source>
        <translation>Asimétrico 8x8</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3814"/>
        <source>Asimètric - superior</source>
        <translatorcomment>Asimétrico - superior</translatorcomment>
        <translation>Asimétrico - superior</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3816"/>
        <source>Clàssic - central</source>
        <translatorcomment>Clásico - central</translatorcomment>
        <translation>Clásico - central</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3820"/>
        <location filename="../scr/frmprincipal.cpp" line="3881"/>
        <source>Clàssic - quadrat central</source>
        <translatorcomment>Clásico - cuadrado central</translatorcomment>
        <translation>Clásico - cadrado central</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3821"/>
        <source>Clàssic - rectangle central</source>
        <translatorcomment>Clásico - rectágulo central</translatorcomment>
        <translation>Clásico - rectágulo central</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3822"/>
        <source>Clàssic - arbre</source>
        <translatorcomment>Clásico - árbol</translatorcomment>
        <translation>Clásico - árbore</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3828"/>
        <source>Wiegleb - clàssic</source>
        <translatorcomment>Wiegleb - clásico</translatorcomment>
        <translation>Wiegleb - clásico</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3830"/>
        <source>Solitari 6x6</source>
        <translation>Solitario 6x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3834"/>
        <source>Anglès antic</source>
        <translation>Inglés antigo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3840"/>
        <source>Wiegleb - fletxa</source>
        <translation>Wiegleb - frecha</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3841"/>
        <source>Clàssic - E</source>
        <translatorcomment>Clásico - E</translatorcomment>
        <translation>Clásico - E</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3842"/>
        <source>Clàssic - R</source>
        <translatorcomment>Clásico - R</translatorcomment>
        <translation>Clásico - R</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3843"/>
        <source>Clàssic - T</source>
        <translatorcomment>Clásico - T</translatorcomment>
        <translation>Clásico - T</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3849"/>
        <source>Triangular 4x7 - piràmide</source>
        <translatorcomment>Triangular 4x7 - pirámide</translatorcomment>
        <translation>Triangular 4x7 - pirámide</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3850"/>
        <source>Quadrat 5x5 - piràmide</source>
        <translatorcomment>Cuadrado 5x5 - pirámide</translatorcomment>
        <translation>Cadrado 5x5 - pirámide</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3852"/>
        <location filename="../scr/frmprincipal.cpp" line="3862"/>
        <source>Asimètric 6x6</source>
        <translation>Asimétrico 6x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3854"/>
        <source>Anglès antic - diamant</source>
        <translatorcomment>Inglés antiguo - diamante</translatorcomment>
        <translation>Inglés antigo - diamante</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3861"/>
        <source>Clàssic - quadrat</source>
        <translatorcomment>Clásico - cuadrado</translatorcomment>
        <translation>Clásico - cadrado</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3865"/>
        <source>Clàssic - cúpula</source>
        <translatorcomment>Clásico - cúpula</translatorcomment>
        <translation>Clásico - cúpula</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3867"/>
        <source>Clàssic - Cabana</source>
        <translatorcomment>Clásico - cabaña</translatorcomment>
        <translation>Clásico - cabana</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3876"/>
        <source>Europeu - quadrat</source>
        <translation>Europeo - cadrado</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3894"/>
        <source>Hexagonal inclinat</source>
        <translation>Hexagonal inclinado</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3895"/>
        <source>Clàssic - 4 forquilles</source>
        <translatorcomment>Clásico - 4 forquillas</translatorcomment>
        <translation>Clásico - 4 forquillas</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3896"/>
        <source>Pentagonal</source>
        <translation>Pentagonal</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3898"/>
        <source>Clàssic - Dos quadrats</source>
        <translatorcomment>Clásico - dos cuadrados</translatorcomment>
        <translation>Clásico - dous cadrados</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3899"/>
        <source>Clàssic - Banyes</source>
        <translatorcomment>Clásico - cuernos</translatorcomment>
        <translation>Clásico - cornos</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3900"/>
        <source>Clàssic - X</source>
        <translatorcomment>Clásico - X</translatorcomment>
        <translation>Clásico - X</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3901"/>
        <source>Clàssic - Torxa</source>
        <translatorcomment>Clásico -  antorcha</translatorcomment>
        <translation>Clásico -  facho</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3902"/>
        <source>Clàssic - Palau</source>
        <translatorcomment>Clásico - palacio</translatorcomment>
        <translation>Clásico - palacio</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3904"/>
        <source>Personalitzat</source>
        <translation>Personalizado</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3908"/>
        <source>Solo</source>
        <translation>Solo</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3909"/>
        <source>Solitari 8x3</source>
        <translation>Solitario 8x3</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3910"/>
        <source>Solitari 8x6</source>
        <translation>Solitario 8x6</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="473"/>
        <source>Retrocedeix</source>
        <translation>Retroceder</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="476"/>
        <location filename="../scr/frmprincipal.cpp" line="492"/>
        <location filename="../scr/frmprincipal.cpp" line="580"/>
        <source>Resol</source>
        <translation>Resolve</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="819"/>
        <source>&amp;%1 %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="890"/>
        <source>Credits del %1</source>
        <translation>Créditos das %1</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="945"/>
        <location filename="../scr/frmprincipal.cpp" line="951"/>
        <source>Marques personals</source>
        <translation>Marcas persoais</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1007"/>
        <source>Elimina les marques personals</source>
        <translation>Elimina as marcas persoais</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="1164"/>
        <location filename="../scr/frmprincipal.cpp" line="1170"/>
        <location filename="../scr/frmprincipal.cpp" line="1531"/>
        <location filename="../scr/frmprincipal.cpp" line="2130"/>
        <location filename="../scr/frmprincipal.cpp" line="2385"/>
        <location filename="../scr/frmprincipal.cpp" line="2435"/>
        <location filename="../scr/frmprincipal.cpp" line="2527"/>
        <location filename="../scr/frmprincipal.cpp" line="2700"/>
        <source>Atura</source>
        <translation>Para</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2385"/>
        <source>Desant dades</source>
        <translation>Gardando os datos</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2407"/>
        <source>Desant dades: %1 de %2</source>
        <translation>Gardando os datos: %1 de %2</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2435"/>
        <location filename="../scr/frmprincipal.cpp" line="2527"/>
        <location filename="../scr/frmprincipal.cpp" line="2700"/>
        <source>Carregant dades</source>
        <translation>Cargando os datos</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2736"/>
        <location filename="../scr/frmprincipal.cpp" line="2961"/>
        <source>No hi ha solucions</source>
        <translation>Non hai solucións</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2761"/>
        <source>Carregar </source>
        <translation>Cargar </translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2768"/>
        <source>S&apos;ha carregat la </source>
        <translation>Cargouse a </translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="2897"/>
        <source>Moviment </source>
        <translation>Movemento </translation>
    </message>
    <message>
        <source> de </source>
        <translation type="vanished"> de </translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3633"/>
        <source>Cap joc personalitzat</source>
        <translation>Sen xogos personalizados</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3771"/>
        <source> - invers</source>
        <translation> - inverso</translation>
    </message>
    <message>
        <location filename="../scr/frmprincipal.cpp" line="3774"/>
        <source> - diagonal</source>
        <translation> - diagonal</translation>
    </message>
</context>
</TS>
